package totalPartitioner_withSampling;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

public class MyReducer extends Reducer<Text, Text, Text, Text> {
	private MultipleOutputs mos;
	private	StringBuilder sb;
	private	Text doc = new Text();
	private	Text list=new Text();
	private Text result = new Text();
	private String[] arr;
	public void setup(Context context) {
		mos = new MultipleOutputs(context);
              	sb = new StringBuilder();

	}

	public void cleanup(Context context) throws IOException, InterruptedException {
		mos.close();
	}

	@Override
	protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
		// here we print the document_id -> document length
		
		if (key.toString().equals("_@0")) {
			for (Text val:values) {
				arr = val.toString().split("@");//.toString().substring(2);
				doc.set(arr[0]);
				result.set(arr[1]);
				mos.write("docToLen", doc, result,"docToLen/docToLen");
			}
		} else {
			//StringBuilder sb = new StringBuilder();
			sb.append("{");
			// here we print the posting list i.e a term_id and value list with
			// [doc_id:freq]
			for (Text val:values) {
				arr=val.toString().split("@");
				sb.append(arr[0]);
				sb.append(":");
				sb.append(arr[1]);
				sb.append(",");
			}
			sb.deleteCharAt(sb.length() - 1 );
			sb.append("}");


			list.set(sb.toString());
			mos.write("postingList", key.toString().split("@")[0],list,"postingList/postingList");
			sb.setLength(0);
		}

	}
}

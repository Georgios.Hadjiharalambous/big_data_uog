package totalPartitioner_withSampling;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import org.apache.hadoop.mapreduce.Counters;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.TaskCounter;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.KeyValueTextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.mapreduce.lib.partition.InputSampler;
import org.apache.hadoop.mapreduce.lib.partition.TotalOrderPartitioner;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import totalPartitioner_withSampling.MyMapper.Counters_index;

import java.io.File;
import java.io.FileWriter;
import java.net.URI;


public class MyIndexer extends Configured implements Tool {
	@Override
	public int run(String[] args) throws Exception {
		Configuration myconf = getConf();
		Path partitionInputPath = new Path("./sampling");//./sampling for hdfs
		Path partitionOutputPath = new Path("./partition");//./partition for hdfs

		FileSystem fs = FileSystem.get(myconf);
		fs.copyFromLocalFile(new Path("file:///users/pgt/2486083h/big_data/src/main/resources/stopword-list.txt"),
				new Path("hdfs://bigdata-10.dcs.gla.ac.uk:8020/user/2486083h/stopword-list.txt"));

		fs.copyFromLocalFile(new Path("file:///users/pgt/2486083h/big_data/src/main/resources/results_for_sampling.txt"),
				new Path("hdfs://bigdata-10.dcs.gla.ac.uk:8020/user/2486083h/sampling/results_for_sampling.txt"));
		/* 
		 * Sampling Job
		 */

		Job samplingJob = Job.getInstance(myconf, "Sampling from sample result");
		samplingJob.setJarByClass(MyIndexer.class);

		// The following instructions need to be executed before writing the partition file
		//samplingJob.setNumReduceTasks(nbReducers);
		FileInputFormat.setInputPaths(samplingJob, partitionInputPath);
		FileOutputFormat.setOutputPath(samplingJob, partitionOutputPath);
		TotalOrderPartitioner.setPartitionFile(samplingJob.getConfiguration(), partitionOutputPath);
		samplingJob.setInputFormatClass(KeyValueTextInputFormat.class);
		samplingJob.setMapOutputKeyClass(Text.class);
		samplingJob.setMapOutputValueClass(Text.class);

		// Write partition file with random sampler
		InputSampler.Sampler<Text, Text> sampler = new InputSampler.RandomSampler<>(1, 40000);
		InputSampler.writePartitionFile(samplingJob, sampler);
		/*
		 * MapReduce Job
		 */

		//myconf = getConf();

		Job mrJob = Job.getInstance(new Configuration(), "Index creation and total sorting on terms");

		mrJob.addCacheFile(new URI("./stopword-list.txt"));
		mrJob.addCacheFile(new URI("./sampling/results_for_sampling.txt"));

		myconf.set("textinputformat.record.delimiter", "\n[[");

		mrJob.setJarByClass(MyIndexer.class);

		// Partitioning configuration
		mrJob.setPartitionerClass(TotalOrderPartitioner.class);
		TotalOrderPartitioner.setPartitionFile(mrJob.getConfiguration(), partitionOutputPath);

		mrJob.addCacheFile(partitionOutputPath.toUri());
		mrJob.setSortComparatorClass(Comparator.class);
		mrJob.setGroupingComparatorClass(CustomGroupComparator.class);
		// Mapper configuration
		mrJob.setMapperClass(MyMapper.class);
		mrJob.setMapOutputKeyClass(Text.class);
		mrJob.setMapOutputValueClass(Text.class);

		// Reducer configuration
		mrJob.setReducerClass(MyReducer.class);
		mrJob.setOutputKeyClass(Text.class);
		mrJob.setOutputValueClass(Text.class);

		MultipleOutputs.addNamedOutput(mrJob, "postingList", TextOutputFormat.class, Text.class, Text.class);
		MultipleOutputs.addNamedOutput(mrJob, "docToLen", TextOutputFormat.class, Text.class, Text.class);
		FileInputFormat.setInputPaths(mrJob, new Path(args[0]));
		FileOutputFormat.setOutputPath(mrJob, new Path(args[1]));

		int res= mrJob.waitForCompletion(true) ? 0 : 1;
		//counters
		Counters cn=mrJob.getCounters();
		Long num_of_tokens=cn.findCounter(Counters_index.NUM_TOKENS).getValue();
		Long num_of_docs=cn.findCounter(TaskCounter.MAP_INPUT_RECORDS).getValue();
		Double avg_doc_length=(num_of_tokens*1.0)/num_of_docs;
		System.out.println("Average document length = "+avg_doc_length);
		System.out.println("Total number of documents = "+num_of_docs);				
		return res;
	}




	public static void main(String[] args) throws Exception {
		System.exit(ToolRunner.run(new MyIndexer(), args));
	}
}

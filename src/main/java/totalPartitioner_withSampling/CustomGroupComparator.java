package totalPartitioner_withSampling;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;

public class CustomGroupComparator extends WritableComparator {
	public CustomGroupComparator() {
		super(Text.class, true);
	}
	@Override
	public int compare(WritableComparable wc1, WritableComparable wc2) {
		String arr1[]=((Text) wc1).toString().split("@");
		String arr2[]=((Text) wc2).toString().split("@");
		return arr1[0].compareTo(arr2[0]);

	}
}

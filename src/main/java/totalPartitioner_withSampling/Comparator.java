package totalPartitioner_withSampling;

import org.apache.hadoop.io.Text;

import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableComparator;
 
public class Comparator extends WritableComparator {
 
    public Comparator() {
        super(Text.class, true);
    }
    
    @SuppressWarnings("rawtypes")
    @Override
    public int compare(WritableComparable wc1, WritableComparable wc2) {
        
    	String arr1[]=((Text) wc1).toString().split("@");
    	String arr2[]=((Text) wc2).toString().split("@");
    	
    	int stateCmp = arr1[0].compareTo(arr2[0]);
		if (stateCmp != 0) {
			return stateCmp;
		} else {
			return  (int)(-Long.parseLong(arr1[1]) + Long.parseLong(arr2[1]));
		}
    }
    
 
}

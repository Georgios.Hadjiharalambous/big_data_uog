package totalPartitioner_withSampling;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import utils.PorterStemmer;

public class MyMapper extends Mapper<LongWritable, Text, Text, Text> {


	private static PorterStemmer stemmer = new PorterStemmer();

	private Text word = new Text();
	private Text value = new Text();
	StringBuilder sb = new StringBuilder();

	static enum Counters_index {
		 NUM_TOKENS
	}

	HashSet<String> stopWords = new HashSet<String>();

	@Override
	protected void setup(Context context) throws IOException, InterruptedException {

		URI[] cacheFiles = context.getCacheFiles();

		if (cacheFiles != null && cacheFiles.length > 0) {
			try {
				String line = "";
				FileSystem fs = FileSystem.get(context.getConfiguration());
				Path getFilePath = new Path(cacheFiles[0].toString());
				BufferedReader reader = new BufferedReader(new InputStreamReader(fs.open(getFilePath)));
				while ((line = reader.readLine()) != null) {
					String[] words = line.split(" ");

					for (int i = 0; i < words.length; i++) {
						// add the words to ArrayList
						stopWords.add(words[i].toLowerCase());
					}
				}
			}

			catch (Exception e) {
				System.out.println("Unable to read the File");
				System.exit(1);
			}
		}
	}

	protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
		//keyPair = new customKey();
		HashMap<String, Integer> tf = new HashMap<>();
		ArrayList<String> tokens = new ArrayList<>();
		int count = 0;
		String[] words = value.toString().toLowerCase().replaceAll("[^a-z]", " ").split("\\s+");
		for (String term : words) {
			term=term.trim();

			if (stopWords.contains(term) || term.equals(""))
				continue;
			char word[] = term.toCharArray();
			stemmer.add(word, word.length);
			stemmer.stem();
			tokens.add(stemmer.toString());
		}

		for (String term : tokens) {
			count++;
			if (tf.containsKey(term))
				tf.put(term, tf.get(term) + 1);
			else {
				tf.put(term, 1);
			}
		}
		for (Map.Entry<String, Integer> entry : tf.entrySet()) {
			sb.append(entry.getKey());sb.append("@");sb.append(entry.getValue());
			word.set(sb.toString());
			sb.setLength(0);
			sb.append(key.toString());sb.append("@");sb.append(entry.getValue());
			value.set(sb.toString());
			context.write(word, value);
			sb.setLength(0);
		}
		word.set("_@"+key.toString());
		sb.append(key.toString());sb.append("@");sb.append(count);
		
		value.set(sb.toString());//doc id , len
		context.write(word, value);
		sb.setLength(0);

		context.getCounter(Counters_index.NUM_TOKENS).increment(count);
	}

}

package totalPartitioner_twoMR;

import java.io.IOException;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class MyMapperSort extends Mapper<Text, Text, Text, Text> {
//identity mapper to emit the output in order for the sampler to sample the neccessary text
	protected void map(Text key, Text value, Context context) throws IOException, InterruptedException {

		context.write(key, value);			
	}
}

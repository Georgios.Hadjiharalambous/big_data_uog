package totalPartitioner_twoMR;

import java.io.IOException;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class MyReducerSort extends Reducer<Text, Text, Text, Text> {
	//identity reducer, for each term it outputs its posting list, there must be exactly one value in the list
	@Override
	protected void reduce(Text key, Iterable<Text> values, Context context) throws IOException, InterruptedException {
		for (Text t:values)
			context.write(key, t);

	}
}

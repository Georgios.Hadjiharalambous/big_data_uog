To run this code you must have access to hdfs server of UOG.

To run this file just write in a terminal

$cd big_data_uog

$mvn clean package

$hadoop totalPartitioner_twoMR.MyIndexer /path/to/input /path/to/output
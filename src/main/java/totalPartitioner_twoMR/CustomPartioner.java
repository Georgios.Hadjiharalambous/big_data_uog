package totalPartitioner_twoMR;


import org.apache.hadoop.mapreduce.Partitioner;

public class CustomPartioner extends Partitioner<CustomKey,FreqDocPair> {

	@Override
	public int getPartition(CustomKey key, FreqDocPair value, int numPartitions) {
		return Math.abs(key.key.hashCode() & Integer.MAX_VALUE) % numPartitions;
	}
}
package totalPartitioner_twoMR;

import java.io.IOException;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;

public class MyReducer extends Reducer<CustomKey, FreqDocPair, Text, Text> {
	private MultipleOutputs mos;
	private	StringBuilder sb;
	private	Long doc;
	private	Text list=new Text();
	private LongWritable result = new LongWritable();
	public void setup(Context context) {
		mos = new MultipleOutputs(context);
              	sb = new StringBuilder();

	}

	public void cleanup(Context context) throws IOException, InterruptedException {
		mos.close();
	}

	@Override
	protected void reduce(CustomKey key, Iterable<FreqDocPair> values, Context context) throws IOException, InterruptedException {
		// here we print the document_id -> document length

		if (key.key.equals("___")) {
			for (FreqDocPair val:values) {
				doc = val.doc_id;//.toString().substring(2);
				result.set(val.freq);
				mos.write("docToLen", doc, result,"docToLen/docToLen");
			}
		} else {
			//StringBuilder sb = new StringBuilder();
			sb.append("{");
			// here we print the posting list i.e a term_id and value list with
			// [doc_id:freq]
			for (FreqDocPair val:values) {
				sb.append(val.doc_id.toString());
				sb.append(":");
				sb.append(val.freq.toString());
				sb.append(",");
			}
			sb.deleteCharAt(sb.length() - 1 );
			sb.append("}");


			list.set(sb.toString());
			mos.write("postingList", key.key,list,"postingList/postingList");
			sb.setLength(0);
		}

	}
}
